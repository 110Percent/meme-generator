## Curtis' Meme Generator

This project is my summative for my ICS4U Grade 12 Computer Science class.

The program, presented as a website, serves as a tool to create [image macros](https://en.wikipedia.org/wiki/Image_macro) from pre-existing images, allowing the user to edit captions and other postprocessing effects in real time.

![](https://owo.whats-th.is/22288e.png)
(Above screenshot represents in-development appearance and is subject to change.)  
  


### Languages
  
The backend for this meme generator uses Node.js, a server-side JavaScript environment, running a webserver framework known as [Express](https://expressjs.com/).  
The frontend, made for web browsers, uses [Pug](https://pugjs.org/api/getting-started.html) (a templating engine for Node.js), CSS and Javascript.  

### Directions

* Downloading the project
    * You can download this project either through git or the download (cloud with arrow) icon in the top bar of this page.

* Starting the server
    * Ensure you have Node.js and NPM installed.
    * Navigate to the project's directory using a terminal.
    * Enter `npm install` to install the required dependencies.
    * Enter `npm start` to start the webserver.

* On a web browser
    * Navgate to `localhost:3000`. This will load a webpage, where you will find the meme generator.
    * Main
        * Put an HTTP(s) URL of an image into the `Background Image URL` box to set the image as the background for the macro.
        * Enter the top and bottom captions into the respective textboxes to add them to the macro.
    * Optional 
        * Use the thickness slider to change the width of the text's outline.
        * Check the checkbox to add a watermark to the bottom of the image.
        * Use the deep fry slider to add a "deep-fry" effect to the image, increasing the saturation and contrast of the macro.

  
  
### How it works

#### Express
  
As mentioned above, this project uses a framework called Express. This framework is widely used among developers creating webservers in Node.js, as it provides an extensive way to dynamically show webpages, rather more statically-oriented servers such as Nginx and Apache.   
  
#### Pug
  
Rather than use raw HTML to be stored, the webpage is spread across `.pug` files, to be interpreted by the Pug templating engine. Using Pug rather than HTML was much simpler for me, allowing me to write the page in a short, compact and concise format without the hassle of angle brackets and formatting.  
  
The base layout for the page is stored in `layout.pug`, which maintains the positioning of the header, control panel, canvas, footer and scripts on the page. Placeholders called "blocks" are found in this file, which are filled by the other pug files used by the server. The "error" block is only used in the case of an error (such as a 404), whereas the header, panel, canvas and scripts panel are used for the main page (`index.pug`). These blocks are filled by the site title/subtitle, the meme control panel, the canvas where the meme is drawn and the page's JavaScript, respectively.  
  
The page is filled out with the correct content, style and scripts and is then translated into HTML by the templating engine. This newly-generated HTML (and other code) is sent to the browser, which renders as a normal webpage would.  
  

#### HTML5 Canvas  
  
Canvas is a graphics processing and rendering engine included in the HTML5 standard, the standard for modern web browsers. The engine allows developers to draw graphics to an element in the DOM in a manner similar to the Allegro framework we used during the course. Canvas elements are used as wrappers to store and display the graphics assigned via the JavaScript embedded in the page.  
