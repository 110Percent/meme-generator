// Get the canvas from the webpage and start the canvas framework on that element
let canvas = document.getElementById('canvas'),
    ctx = canvas.getContext('2d'),
    backgroundImg = new Image; // Create a new image for the background

// Declare variables for use with creating the meme
let backURL, topText, bottomText, lineThickness, h, w, fontSize, bottomLines, iFunny, deepFry, putFaces;

// Elements to show slider values on the panel
let displayThickness = document.getElementById('showThickness'),
    displayDeepfry = document.getElementById('showDeepfry');

// Create new assets (watermarks, etc)
let assets = {
    ifunny: new Image
};
assets.ifunny.src = '/img/ifunny.png';
assets.ifunny.crossOrigin = "anonymous";

// Update the meme every 50 seconds
setInterval(drawMeme, 50);


// Main function to draw the meme
function drawMeme(ref) {

    // Get values from webpage panel
    backURL = document.getElementById('imgUrl').value;
    topText = document.getElementById('topText').value;
    bottomText = document.getElementById('bottomText').value;
    lineThickness = document.getElementById('thickness').value;
    deepFry = document.getElementById('deepfry').value;
    iFunny = document.getElementById('ifunny').checked;

    // Set display text next to sliders
    displayThickness.textContent = lineThickness;
    displayDeepfry.textContent = deepFry;

    // Change internal deepFry value for use with image processing
    deepFry = deepFry / 20 + 1


    // If the background image URL has changed, update it 
    if (backgroundImg.src != backURL) {
        backgroundImg.src = backURL;
        backgroundImg.crossOrigin = "anonymous";
    }


    // Fix the image size to fit an 800x600 box

    // If the image is smaller than the required amount, enlarge it to fit the box.
    if (h < 600 && w < 800) {
        w = (w / h) * 600;
        h = 600;
    }

    // If the image is too wide, shrink it down to fit the proper width
    if (backgroundImg.width > 800) {
        h = (backgroundImg.height / backgroundImg.width) * 800;
        w = 800;
    } else {
        h = backgroundImg.height;
        w = backgroundImg.width;
    }

    // If the image is too tall, shrink it further to make it the right height
    if (backgroundImg.height > 600) {
        h = 600;
        w = (backgroundImg.width / backgroundImg.height) * 600;
    } else {
        h = backgroundImg.height;
        w = backgroundImg.width;
    }

    // Set the canvas dimensions to fix that of the image
    canvas.width = w;
    canvas.style.width = w + 'px';
    canvas.height = h;
    canvas.style.height = h + 'px';


    // Fill the canvas with black (in case an image with a transparent background is used)
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    // Apply the appropriate effects to the canvas for the "deep fry" value
    ctx.filter = `saturate(${deepFry * 5 - 4}) contrast(${deepFry / 4 + 0.75})`;

    // Draw the background image to the canvas
    ctx.drawImage(backgroundImg, 0, 0, w, h);

    // Set the initial font size based off the height of the image
    fontSize = Math.floor(h / 10);
    //Shrink the font size according to how many lines the top text of the meme will have
    fontSize = fontSize * Math.pow(0.8, getLines(ctx, topText, w, fontSize).length);
    // Draw the top text to the screen
    write(topText, fontSize, fontSize, lineThickness);

    // Repeat the process for the bottom text
    fontSize = Math.floor(h / 10);
    fontSize = fontSize * Math.pow(0.8, getLines(ctx, bottomText, w, fontSize).length);
    write(bottomText, h - (fontSize * bottomLines), fontSize, lineThickness, true);

    // If the iFunny box is checked, draw the watermark at the bottom of the canvas
    if (iFunny) {

        // Rectangle covering the bottom of the image
        ctx.fillStyle = '#141416';
        ctx.fillRect(0, h - 20, w, 20);
        // Watermark at bottom-right
        ctx.drawImage(assets.ifunny, w - 135, h - 20);

    }

}

function write(text, ypos, size, thickness, isBottom) {

    ctx.textBaseline = 'middle';
    ctx.textAlign = 'center';

    // The text written to the screen always uses the Impact font
    ctx.font = size + "px Impact"

    // Add a space between each character to increase the kerning
    text = text.split('').join(String.fromCharCode(8202));

    // The stroke width is passed in from the panel slider
    ctx.lineWidth = thickness;

    // Find out how many lines will be drawn to the screen
    let lines = getLines(ctx, text, w * 0.9, size);

    // If this is the bottom text, set the amount of lines for the bottom text to use for positioning
    if (isBottom) bottomLines = lines.length;

    // Draw each line for the desired text
    for (let i = 0; i < lines.length; i++) {

        // Draw the outline for the text (in black)
        ctx.strokeStyle = 'black';
        ctx.strokeText(lines[i], w / 2, ypos + (i * size * 1.2));

        // Fill the text with white
        ctx.fillStyle = 'white';
        ctx.fillText(lines[i], w / 2, ypos + (i * size * 1.2));
    }
}

function getLines(ctx, phrase, maxPxLength, size) {

    // Declare variables
    let wordArray = phrase.split(' '),
        lineArray = [],
        lastLine = wordArray[0],
        measure = 0;

    // If there's only one word, then it can't be wrapped
    if (wordArray.length < 2) return wordArray

    // Set the testing font size
    ctx.font = size + "px Impact"

    // Repeat for each word in the phrase
    for (let i = 1; i < wordArray.length; i++) {

        // Measure the length for each word added onto the current line
        let w = wordArray[i];
        measure = ctx.measureText(lastLine + ' ' + w).width;

        // If the word fits in the width boundary, add it to the line
        if (measure < maxPxLength) {
            lastLine += (' ' + w);
        } else {
            // Otherwise, add it to a new line
            lineArray.push(lastLine);
            lastLine = w;
        }

        // If this is the last line and there are no more lines left, push the line to the array
        if (i == wordArray.length - 1) {
            lineArray.push(lastLine);
            break;
        }
    }

    // Return the array of wrapped phrases
    return lineArray;
}